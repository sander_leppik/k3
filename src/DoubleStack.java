import java.util.LinkedList;

public class DoubleStack {

   private LinkedList<Double> StackList;


   public static void main (String[] argum) {
      DoubleStack Stack = new DoubleStack();
      DoubleStack Stack2 = new DoubleStack();

      System.out.println("Not the real test");
//      Stack.push(5.0);
//      Stack.push(4.0);
//      Stack.push(3.0);
//      Stack.push(2.0);
//      Stack.push(1.0);
//      System.out.println(Stack.StackList);
//      System.out.println(Stack);
//
//      Stack2.push(5.0);
//      Stack2.push(4.0);
//      Stack2.push(3.0);
//      Stack2.push(2.0);
//      Stack2.push(1.0);

      Stack.push(5.0);
      Stack.push(4.0);
      Stack.push(3.0);
      Stack.push(2.0);
      Stack.push(1.0);

      System.out.println(DoubleStack.interpret("2 5 9 ROT + SWAP -")
      );

   }

   DoubleStack() {
      StackList = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack Clone = new DoubleStack();
      Double tempDouble;

      // Iterator might be faster
      int stackSize = StackList.size();
      for(int i = 0; i < stackSize; i++){
         tempDouble = StackList.pop();
         StackList.add(stackSize - 1, tempDouble);
         Clone.StackList.add(tempDouble);
      }
      return Clone;
   }

   public boolean stEmpty() {

      return StackList.size() == 0;
   }

   public void push (double a) {
      StackList.add(0, a);
   }

   public double pop() {
      return StackList.pop();
   }

   public void op (String s) {
      double first = pop();
      double second = pop();

      switch (s){
         case "+":
            push(first + second);
            break;
         case "-":
            push(second - first);
            break;
         case "*":
            push(first * second);
            break;
         case "/":
            push(second / first);
            break;
      }
   }
  
   public double tos() {
      return StackList.get(0);
   }

   @Override
   public boolean equals (Object o) {
      if(o instanceof DoubleStack){
         DoubleStack OtherStack = (DoubleStack) o;
         if(OtherStack.StackList.size() == this.StackList.size()){
            DoubleStack OtherCopy = new DoubleStack();
            try {
               OtherCopy = (DoubleStack) OtherStack.clone();
            } catch (CloneNotSupportedException e) {
               e.printStackTrace();
            }

            DoubleStack MainCopy = new DoubleStack();
            try {
               MainCopy = (DoubleStack) this.clone();
            } catch (CloneNotSupportedException e) {
               e.printStackTrace();
            }

            for (int i = 0; i < StackList.size(); i++){
               if(MainCopy.pop() != OtherCopy.pop()){
                  return false;
               }
            }
            return true;
         }
      }
      return false;
   }

   @Override
   public String toString() {
      if(StackList.size() == 0){
         return "";
      }
      DoubleStack Copy = new DoubleStack();
      try {
         Copy = (DoubleStack) this.clone();
      } catch (CloneNotSupportedException e) {
         e.printStackTrace();
      }

      String s = Double.toString(Copy.pop());

      for (double element : Copy.StackList) {
         s = element + ", " + s;
      }
      return s;
   }

   public static double interpret (String pol) {
      String[] polArr = pol.trim().split("\\s+");
      DoubleStack TempStack = new DoubleStack();
      double num2;
      double num1;

      for (String el : polArr) {
         switch (el){
            case "+":
               num2 = TempStack.pop();
               num1 = TempStack.pop();
               TempStack.push(num1 + num2);
               break;

            case "-":
               num2 = TempStack.pop();
               num1 = TempStack.pop();
               TempStack.push(num1 - num2);
               break;
            case "*":
               num2 = TempStack.pop();
               num1 = TempStack.pop();
               TempStack.push(num1 * num2);
               break;
            case "/":
               num2 = TempStack.pop();
               num1 = TempStack.pop();
               TempStack.push(num1 / num2);
               break;
            case "SWAP":
               num2 = TempStack.pop();
               num1 = TempStack.pop();
               TempStack.push(num2);
               TempStack.push(num1);
               break;
            case "ROT":
               num1 = TempStack.pop();
               num2 = TempStack.pop();
               Double num3 = TempStack.pop();

               TempStack.push(num2);
               TempStack.push(num1);
               TempStack.push(num3);

            default:
               try {
                  double temp = Double.parseDouble(el);
                  TempStack.push(temp);
               }catch (RuntimeException e){}
               break;
         }

      }
      if(TempStack.StackList.size() != 1){
         throw new RuntimeException();
      }
      return TempStack.pop();
   }

}